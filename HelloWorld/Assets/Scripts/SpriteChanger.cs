﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {

	public SpriteRenderer sr;
	public Transform tf;
	public speed sp;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			sr.color = Color.blue;
		} else {
			sr.color = Color.white;
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			// Move to the right
			tf.position = tf.position + (Vector3.right * speed);
	}
	}
}

