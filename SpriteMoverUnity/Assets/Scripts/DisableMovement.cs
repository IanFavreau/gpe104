﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//........................................Enables and Disables Player Movement.............................................
		if (Input.GetKey ("p"))  {																			// Reads in player's input of the P Key
			if (GetComponent<Mover> ().enabled == false) {													// Accesses the Mover script and tests if it is disabled
				GetComponent<Mover> ().enabled = true;														// Accesses the Mover script and turns it on so the player can move
			} else if (GetComponent<Mover> ().enabled == true) {											// Accesses the Mover script and tests if it is enabled
				GetComponent<Mover> ().enabled = false;														// Accesses the Mover script and turns it off so the player is not able to move
			}
		}
	}
}
