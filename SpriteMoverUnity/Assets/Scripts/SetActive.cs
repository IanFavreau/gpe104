﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActive : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		//........................................Sets Game Object "Spaceman" to Inactive.............................................
		if (Input.GetKey ("q")) {																			// Reads in player's input of the Q Key
			gameObject.SetActive (false);																	// Sets the GameObject "Spaceman" to inactive, making it unable to be modified further
		}
			
	}
}
