﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {


	}
	// Update is called once per frame
	void Update () {

		//........................................Moves Game Object "Spaceman" to the Origin.............................................
		Transform tf = GetComponent<Transform>();																			// Accesses the inspector's Transform Component

		if (Input.GetKeyDown("space")) {																					// Reads in the player's input of the spacebar
			tf.position = new Vector3(0, 0, 0);																				// Sets the player's position to the origin of the scene.

		}
	}
}
