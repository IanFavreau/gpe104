﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Mover : MonoBehaviour {
																									
	private Transform tf;																			// A private variable is declared which uses the Unity component Transform, naming it "tf"
	public int speed;																				// An Integer "speed" is declared and set to public so that it may be adjusted by the game designer easily

	// Use this for initialization
	void Start () {

		tf = GetComponent<Transform> ();															// Accesses the Transform Componen in the inspector
	}
	
	// Update is called once per frame
	void Update () {

		//........................................Dash System (Non-incrementing Movement, 1 Unity Meter).............................................
		Vector3 movement = tf.position;																// tf.position is renamed to something more simple and descriptive, "movement", and Vector3 is a Unity Directive

		if (Input.GetKey ("d") && (Input.GetKeyDown(KeyCode.LeftShift))) {							// Reads player's input of both the D Key being pressed and the Left Shift Key held down.
			movement.x += 1f;																		// Sets  movement equal to itself, incremented by 1f in the right direction
		}
		if (Input.GetKey ("a") && (Input.GetKeyDown(KeyCode.LeftShift))) {							// Reads player's input of both the A Key being pressed and the Left Shift Key held down.
			movement.x -= 1f;																		// Sets  movement equal to itself, incremented by 1f in the left direction
		}
		if (Input.GetKey ("w") && (Input.GetKeyDown(KeyCode.LeftShift))) {							// Reads player's input of both the W Key being pressed and the Left Shift Key held down.
			movement.y += 1f;																		// Sets  movement equal to itself, incremented by 1f in the up direction
		}
		if (Input.GetKey ("s") && (Input.GetKeyDown(KeyCode.LeftShift))) {							// Reads player's input of both the S Key being pressed and the Left Shift Key held down.
			movement.y -= 1f;																		// Sets  movement equal to itself, incremented by 1f in the down direction
		}


		//........................................Sprinting System (Simultaneous Key Press).............................................
		if (Input.GetKey ("d") && (Input.GetKeyDown (KeyCode.LeftControl))) {						// Reads player's input of both the D Key being pressed and the Left Control Key held down.
			speed = 5;																				// Adjusts the speed integer to 5, thus increasing the speed momentarily
		} else if(Input.GetKeyUp (KeyCode.LeftControl)) {											// This allows the player to cease sprinting once they releas the Left Control Key
			speed = 2;																				// Resets the player's speed to 2
		}
		if (Input.GetKey ("a") && (Input.GetKeyDown (KeyCode.LeftControl))) {						// Reads player's input of both the A Key being pressed and the Left Control Key held down.
			speed = 5;																				// Adjusts the speed integer to 5, thus increasing the speed momentarily
		} else if(Input.GetKeyUp (KeyCode.LeftControl)) {											// This allows the player to cease sprinting once they releas the Left Control Key
			speed = 2;																				// Resets the player's speed to 2
		}
		if (Input.GetKey ("w") && (Input.GetKeyDown (KeyCode.LeftControl))) {						// Reads player's input of both the W Key being pressed and the Left Control Key held down.
			speed = 5;																				// Adjusts the speed integer to 5, thus increasing the speed momentarily
		} else if(Input.GetKeyUp (KeyCode.LeftControl)) {											// This allows the player to cease sprinting once they releas the Left Control Key
			speed = 2;																				// Resets the player's speed to 2
		}
		if (Input.GetKey ("s") && (Input.GetKeyDown (KeyCode.LeftControl))) {						// Reads player's input of both the S Key being pressed and the Left Control Key held down.
			speed = 5;																				// Adjusts the speed integer to 5, thus increasing the speed momentarily
		} else if(Input.GetKeyUp (KeyCode.LeftControl)) {											// This allows the player to cease sprinting once they releas the Left Control Key
			speed = 2;																				// Resets the player's speed to 2
		}

		//........................................Basic Movement.............................................
		if (Input.GetKey ("d")) {																	// Reads player's input of the D Key
			movement.x += .05f * speed;																// Incrementally adjusts the player's position by .01f in the right direction and multiplied by speed to relate the integer
		}
		if (Input.GetKey ("a")) {																	// Reads player's input of the A Key
			movement.x -= .05f * speed;																// Incrementally adjusts the player's position by .01f in the left direction and multiplied by speed to relate the integer
		}
		if (Input.GetKey ("w")) {																	// Reads player's input of the W Key
			movement.y += .05f * speed;																// Incrementally adjusts the player's position by .01f in the up direction and multiplied by speed to relate the integer
		}
		if (Input.GetKey ("s")) {																	// Reads player's input of the S Key
			movement.y -= .05f * speed;																// Incrementally adjusts the player's position by .01f in the down direction and multiplied by speed to relate the integer
		}



		tf.position = movement;																		// sets tf.position equal to movement so that it is a moredescriptive variable name

	}


}

