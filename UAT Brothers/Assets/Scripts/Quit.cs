﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//........................................Quits the Game (Only in Build).............................................
		if (Input.GetKey ("escape")) {																		// Reads in player's input of the Escape Key
			Application.Quit ();																			// Utilizes Application.Quit internal command to close the Unity Scene after it is built
		}
	}
}
