﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootSteps : MonoBehaviour {

	public AudioClip FootStepAudio;
	public AudioSource FootStepsSource;

	// Use this for initialization
	void Start () {
		FootStepsSource.clip = FootStepAudio;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("d")) {
			FootStepsSource.Play ();
		}

		if (Input.GetKeyDown ("a")) {
			FootStepsSource.Play ();
		}

		if (Input.GetKeyUp ("d")) {
			FootStepsSource.Stop ();
		}

		if (Input.GetKeyUp ("a")) {
			FootStepsSource.Stop ();
		}
	}
}
