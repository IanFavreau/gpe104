﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotPawn : Pawn {

	private Rigidbody2D rb;
	private SpriteRenderer sr;
	private Transform tf;
	private Animator anim;
	private Rigidbody2D m_Rigidbody;

	public float moveSpeed;
	public float jumpForce;
	public bool isGrounded;
	public float groundDistance = 0.1f;

	public void Start() {
		// Get my components
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
		anim = GetComponent<Animator> ();

		m_Rigidbody = GetComponent<Rigidbody2D>();

	}

	public void Update() {
		// Check for Grounded
		CheckForGrounded();

		if (Input.GetKeyDown ("d")) {																			// Reads in the player's D Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
		}
		if (Input.GetKeyUp ("d")) {																				// Reads in the player's D Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
		}
		if (Input.GetKeyDown ("a")) {																			// Reads in the player's A Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
		}
		if (Input.GetKeyUp ("a")) {																				// Reads in the player's A Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
		}

		if (Input.GetKeyDown ("s")) {																			// Reads in the player's S Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
		}
		if (Input.GetKeyUp ("s")) {																				// Reads in the player's S Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
		}

		if (Input.GetKeyDown ("w")) {																			// Reads in the player's W Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
		}
		if (Input.GetKeyUp ("w")) {																				// Reads in the player's W Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
		}


		m_Rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

	}

	public void CheckForGrounded () {
		RaycastHit2D hitInfo = Physics2D.Raycast (tf.position, Vector3.down, groundDistance);
		if (hitInfo.collider == null) {
			isGrounded = false;
		} else {
			isGrounded = true;
			Debug.Log ("Standing on " + hitInfo.collider.name);
		}
	}

	public override void Move (Vector2 moveVector) {
		// Change X velocity based on speed and moveVector
		rb.velocity = new Vector2 (moveVector.x * moveSpeed, rb.velocity.y);

		// If velocity is <0, flip sprite. 
		if (rb.velocity.x < 0) {
			anim.Play ("RobotWalk");
			sr.flipX = true;
		} else if (rb.velocity.x > 0) {
			anim.Play ("RobotWalk");
			sr.flipX = false;
		} else {
			anim.Play ("RobotIdle");
		}
	}

	public override void Jump () {
		if (isGrounded) {
			rb.AddForce (Vector2.up * jumpForce, ForceMode2D.Force);
		}
	}

}



