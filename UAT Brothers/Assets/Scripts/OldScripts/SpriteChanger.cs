﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {

	public SpriteRenderer sr;																			// Declares public variable sr from the inspector's Sprite Renderer

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();															// Accesses the Sprite Renderer and sets it equal to the shorter variabl;e nam "sr"
	}

	// Update is called once per frame
	void Update () {

		//........................................Color Changer.............................................
		if (Input.GetKey ("c")) {																		// Reads in the player input of the C Key
			StartCoroutine ("waitOneSecond");															// References the StartCoroutine function to start it's chain of events (iterator)
		
		} else {
			sr.color = Color.white;																		// Defaults back to white
		}

		//........................................Mono-Behavior Color Changer.............................................
		if (Input.GetKey ("o")) {																		// Reads in the player input of the O Key
			sr.color = Color.green;
		} else {
			sr.color = Color.white;																		// Defaults back to white
		}
	}

		IEnumerator waitOneSecond() {
		yield return new WaitForSeconds(1);																// Uses WaitForSeconds command so that the series of colors may be displayed at different times to not overlap
		sr.color = Color.red;																			// Changes the Spaceman Sprite to red
		yield return new WaitForSeconds(1);																// Uses WaitForSeconds command so that the series of colors may be displayed at different times to not overlap
		sr.color = Color.green;																			// Changes the Spaceman Sprite to Green
		yield return new WaitForSeconds(1);																// Uses WaitForSeconds command so that the series of colors may be displayed at different times to not overlap
		sr.color = Color.blue;																			// Changes the Spaceman Sprite to blue
		yield return new WaitForSeconds(1);																// Uses WaitForSeconds command so that the series of colors may be displayed at different times to not overlap
		sr.color = Color.white;																			// Defaults back to white
		yield return new WaitForSeconds(1);																// Uses WaitForSeconds command so that the series of colors may be displayed at different times to not overlap

	
	
	}

	}

