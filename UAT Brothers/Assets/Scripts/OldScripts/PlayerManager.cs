﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	Animator anim;																								// Truncates the Animator name and assignsit to "anim"

	// Use this for initialization
	void Start ()  {
		anim = GetComponent<Animator> ();																		// Accesses the inspector's Animator Component
	}
	
	// Update is called once per frame
	void Update () {

		//........................................Animation Control (Idle to Walking to Idle).............................................
		if (Input.GetKeyDown ("d")) {																			// Reads in the player's D Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
		}
		if (Input.GetKeyUp ("d")) {																				// Reads in the player's D Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
			}
		if (Input.GetKeyDown ("a")) {																			// Reads in the player's A Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
			}
		if (Input.GetKeyUp ("a")) {																				// Reads in the player's A Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
			}

		if (Input.GetKeyDown ("s")) {																			// Reads in the player's S Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
			}
		if (Input.GetKeyUp ("s")) {																				// Reads in the player's S Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
			}

		if (Input.GetKeyDown ("w")) {																			// Reads in the player's W Key
			anim.SetInteger ("State", 1);																		// Accesses the State Condition State 1
			}
		if (Input.GetKeyUp ("w")) {																				// Reads in the player's W Key
			anim.SetInteger ("State", 0);																		// Accesses the State Condition State 0
			}


		}
	}
