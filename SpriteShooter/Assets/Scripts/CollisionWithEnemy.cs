﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithEnemy : MonoBehaviour {


	private Rigidbody2D rb;											// sets rb to private of type "RigidBody2D", which is a component
	public GameObject other;										// sets other to public of type "GameObject", which is a component
	private SpriteRenderer sr;										// sets sr to private of type "SpriteRednderer", which is a component
	private LifeManager lifeSystem;									// setslifeSystem to private of type "LifeManager", which is a script


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();							// accesses and assigns rb to the component "RigidBody2D" so that it may have a solid structure
		sr = GetComponent<SpriteRenderer>();						// accesses and assigns sr to the component "SpriteRenderer" so that it may have a sprite that appears in the scene
		lifeSystem = FindObjectOfType<LifeManager> ();				// accesses and assigns lifeSystem to the script "LifeManager" so that it may have access to the text within the canvas
	}

	// Update is called once per frame
	void Update () {
		
	}


	void OnCollisionEnter2D(Collision2D other) {					// checks whether the game object has collided with another object

		Destroy(other.gameObject);									// destroys the other game objbect


		if (other.gameObject.name == "TankPrefab") {				// checks if the game object that was destroyed is the Tank (player)
			
			lifeSystem.TakeLife();									// accesses the script to take away lives fro the text in the canvas

		}

		}
}
