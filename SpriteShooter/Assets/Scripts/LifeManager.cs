﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour {

	public int startingLives;							// sets startingLives to public of type "int"
	public int lifeCounter;								// sets lifeCounter to public of type "int"
	private Text textForLives;							// sets startingLives to private of type "Text"


	// Use this for initialization
	void Start () {
		textForLives = GetComponent<Text> ();			// relates textForLives to the accessed Text component
		lifeCounter = startingLives;					// relates lifeCounter to the number of starting lives so that it won't initially go down
	}
	
	// Update is called once per frame
	void Update () {
		textForLives.text = "x " + lifeCounter;			// adjusts the number of lives in the Text component to the UI
	}

	public void TakeLife () {

		lifeCounter--;									// incrementally decreases the player's health upon taking damage
	}


}
