﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public Transform tf;							// sets tf to public of type "Transform", which is a component
	public float speed;								// sets speed to public of type "float"
	public List<GameObject> ignoreMe;				// sets ignoreMe to public of type "List<GameObject>" to make sure the other scripts do not try to apply to this game object
	public float timeAfterSpawn;					// sets timeAfterSpawn to public of type "float"

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();			// accesses and assigns tf to the Transform component
		StartCoroutine(Example());					// initialiszes the system to later be used to wait a given amount of time
	}

	// Update is called once per frame
	void Update () {
		Move ();									// calls the move function
	}

	public void OnTriggerEnter2D (Collider2D otherCollider) {				// checks if the game object has entered another game object

		for (int i = 0; i < ignoreMe.Count; i++) {							// for loop allows for the bullet to destroy others and not unintentionally be destroyed
			if (otherCollider.gameObject == ignoreMe[i]) {
				return; 													// Do nothing and Quit 
			}
		}

		Destroy (otherCollider.gameObject);									// destroys the collider component  of the bullet object
		Destroy (gameObject);												// destroys the object itself
	}

	public void Move () {
		tf.position += tf.right * speed; 									// moves the bullet with respect to the speed variable
	}


	IEnumerator Example() {
		yield return new WaitForSeconds(timeAfterSpawn);					// utilizes the established Coroutine to wait a given amount of time after the bullet has been created
		Destroy(this.gameObject);											// Destroys the bullet upon reaching the number of seconds dictated above
	}

}

