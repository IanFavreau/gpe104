﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPoint : MonoBehaviour {


	public GameObject bulletPrefab;						// sets bulletPrefab to public of type "GameObject", which is a component
	public Transform shootPoint;						// sets shootPoint to public of type "Transform", which is a component


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) 											// reads in the players input of the space key
			Instantiate (bulletPrefab, shootPoint.position, shootPoint.rotation);		// creates a bullet from the shootPoint with respect to its rotation and position
		}


	}