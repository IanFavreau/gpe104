﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatSeeker : MonoBehaviour {

	public Transform targetTf;								// sets targetTf to public of type "Transform", which is a component
	public float speed;										// sets speed to public of type "float"
	private Transform tf;									// sets tf to public of type "Transform", which is a component
	public bool isAlwaysSeeking;							// sets isAlwaysSeeking to public of type "bool" so that the game developer can tick a box if they wish for an object to follow the player at all times
	private Vector3 movementVector;							// sets offset to private of type "Vector3"
	public bool isDirectional;								// sets isDirectional to public of type "bool" so that the game developer can tick a box if they wish for an object to appear as though it is looking at the player

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();					// accesses and assigns Transform to tf
		movementVector = targetTf.position - tf.position;	// establishes a path for the object to follow
	}

	// Update is called once per frame
	void Update () {
		if (isAlwaysSeeking) {										// conditional checks if the game object is seeking
			movementVector = targetTf.position - tf.position;		// the same establishing path, but this time it is under the update function and will be called every frame draw
		}

		movementVector.Normalize ();								// esnures the variable stays unchanged
		movementVector *= speed;									// relates speed to the movementVector
		tf.position += movementVector;								// moves the game object along the path

		if (isDirectional) {										// conditional checks if the game object is directional
			tf.right = movementVector;								// uses ".right" because that is the default position of the sprite
		}

	}
}