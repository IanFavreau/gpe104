﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour {



	public GameObject Enemy_Type;     		// The enemy prefab to be spawned.
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	public float spawnTime;					// sets spawnTime as public with type "float"


	void Start () {
		InvokeRepeating ("Spawn", spawnTime, spawnTime);					// repeats the intantiation process

}


	void Update () {



}

	void Spawn() {
		if (GameObject.FindGameObjectsWithTag("Enemy").Length > 2) {		// ensures that no more than three enemies are on the screen at a given time
				return;														// quits and does nothing
			}
				
		//int spawnPointX = Random.Range (-11, 11);
		//int spawnPointY = Random.Range (-9, 9);
		//Vector2 spawnPosition = new Vector2 (spawnPointX, spawnPointY);

		int spawnPointIndex = Random.Range (0, spawnPoints.Length);			// sradnomizes which spawn point the object will originate from



		Instantiate (Enemy_Type, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);		// creates a certain specified enemy type at one of the randomized spawn points
	}
}


