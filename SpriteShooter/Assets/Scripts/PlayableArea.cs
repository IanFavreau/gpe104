﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableArea : MonoBehaviour {

	private Rigidbody2D rb;								// sets rb to private of type "RigidBody2D", which is a component
	public GameObject other;							// sets other to public of type "GameObject", which is a component
	private SpriteRenderer sr;							// sets sr to private of type "SpriteRednderer", which is a component

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();				// accesses and assigns rb to the component "RigidBody2D" so that it may have a solid structure
		sr = GetComponent<SpriteRenderer>();			// accesses and assigns sr to the component "SpriteRenderer" so that it may have a sprite that appears in the scene
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OnTriggerExit2D (Collider2D other){		// checks if any game object has left the collision box
		Destroy(other.gameObject);						// destorys any game object outside of the collision box
		}
	}
