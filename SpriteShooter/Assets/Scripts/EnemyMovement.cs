﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {


	private float latestDirectionChangeTime;									// sets LatestDirectionChangeTime to private of type "float"
	private readonly float directionChangeTime = 3f;							// sets directionChangeTime to private of type "readonly float", equal to 3f
	private float characterVelocity = 2f;										// sets characterVelocity to private of type "float", equal to 2f
	private Vector2 movementDirection;											//sets movementDirection to private of type "Vector2", which is a built-in  Unity functionality
	private Vector2 movementPerSecond;											//sets movementPerSecond to private of type "Vector2", which is a built-in  Unity functionality


	void Start(){
		latestDirectionChangeTime = 0f;											// initializes latestDirectionChangeTime
		calcuateNewMovementVector();											// instantiates calculateNewMovementVector to be used for finding a clear path
	}

	void calcuateNewMovementVector(){
		
		movementDirection = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized; 		// creates a random trajectory with magnitude 1
		movementPerSecond = movementDirection * characterVelocity;												// relates the object's velocity with its direction
	}

	void Update(){
		
		if (Time.time - latestDirectionChangeTime > directionChangeTime){										// if the changeTime was reached, calculate a new movement vector
			latestDirectionChangeTime = Time.time;																// accesses Time to see where it has previously been
			calcuateNewMovementVector();	
		}
			 
		transform.position = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime), 		// moves the object to a random position
			transform.position.y + (movementPerSecond.y * Time.deltaTime));
	}
}

