﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTop : MonoBehaviour {

	public float offset = 0.0f;										// sets offset to public of type "float", equal to 0f
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector3 difference = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;			// determines the trajectory between the mouse's position and the point it originates from
		difference.Normalize ();																				// ensures the variable is unchanged
		float rotation_z = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;							// uses a mathematical process to figure out a rotation procedure
		transform.rotation = Quaternion.Euler (0f, 0f, rotation_z + offset);									// rotates the sprite

	}
}

