﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {


	public GameObject Tank;									// sets Tank to public of type "GameObject", which is a component
	public Transform spawnPoint;							// sets spawnPoint to public of type "Transform", which is a component


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Tank == null) {														// checks if the tank object has been destroyed

			Instantiate (Tank, spawnPoint.position, spawnPoint.rotation);		// creates a new Tank object once the old one has been destroyed

		}
		


			}

	}