﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	
	Quaternion rotation;								// Creates a variable "rotation" of type Quaternion

	void Awake()
	{
		rotation = transform.rotation;					// sets the variable rotation equal to the Transform Component
	}
	void LateUpdate()
	{
		transform.rotation = rotation;					// sets transform.rotation equal to rotation, wchich essentially is an offset from the last function to keep the camera in the same rotation position
	}
}