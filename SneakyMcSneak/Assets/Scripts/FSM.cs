﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour {



	public Transform otherObject;									// Creates a public variable "otherObject" as a Transform Component
	private Transform tf;											// Creates a public variable "tf" as a Transform Component

	public Transform sightStart;									// Creates a public variable "sightStart" as a Transform Component
	public Transform sightEnd;										// Creates a public variable "sightEnd" as a Transform Component
	public bool spotted = false;									// Creates a public variable "spotted" of type bool, initialized to false

	public float rotationSpeed;										// Creates a public variable "tf" as a Transform Component


	public enum AIStates { Patrol, CanSee, CanHear};				// Creates a public variable "AIStates" that takes three parameters of Patrol, CanSee, and CanHear
	public AIStates currentState;									// Creates a public variable "AIStates", initializing it to currentState


	public Transform[] patrolPoints;								// Creates a public variable "patrolPoints" as a Transform Component List

	public float speed;												// Creates a public variable "speed" of type float

	Transform currentPatrolPoint;									// Creates a public variable "ccurrentPatrolPoint" as a Transform Component

	int currentPatrolIndex;											// Creates a public variable "currentPatrolIndex" of type int




		// Use this for initialization
		void Start () {
			tf = GetComponent<Transform> ();											// Accesses the Transform Component and sets it equal to the variable "tf"

			currentPatrolIndex = 0;														// initializes the variable "currentPatrolPoint" to 0

			currentPatrolPoint = patrolPoints [currentPatrolIndex];						// sets the variable "currentPatrolPoint" to patrolPoints in a list


		}

		// Update is called once per frame
		void Update () {

		switch (currentState) {															// sets up a switch statement that will transition between each of the states
		case AIStates.Patrol:															// determines the first state of the AI states, Patrol
				// Do work
			DoPatrol ();																// calls the DoPatrol Function
				// Check for Transitions
			if (Vector3.Distance (tf.position, otherObject.position) < 200) {			// checks if the player is within range
				currentState = AIStates.CanHear;										// transitions the AI state to CanHear
			}
			break;
		case AIStates.CanHear:															// determines the second state of the AI states, Canhear
				// Do work
			DoCanHear ();																// calls the DoCanHear Function
				// Check for Transitions
			if (Vector3.Distance (tf.position, otherObject.position) > 500) {			// checks if the player is out of range
				currentState = AIStates.Patrol;											// transitions the AI state to Patrol
			}

			if (spotted == true) {														// checks to see if the player is detected
				currentState = AIStates.CanSee;											// transitions the AI state to CanSee
			}
			break;
		case AIStates.CanSee:															// determines the final state of the AI states, CanSee
			// Do work
			DoCanSee ();																// calls the DoCanHear Function
			// Check for Transitions
			if (Vector3.Distance (tf.position, otherObject.position) > 500) {			// checks if the player is out of range
				currentState = AIStates.Patrol;											// transitions the AI state to Patrol
			}
			break;
		}

		Raycast();																		// calls the Raycast Function
	}
		
		

	void Raycast () {
		Debug.DrawLine (sightStart.position, sightEnd.position, Color.red);				// draws a phsycial line for the developer to see as well as a line to detect the player (sees the player)
		spotted = Physics2D.Linecast (sightStart.position, sightEnd.position);			// sets the Boolean value to the Raycast using the parameters of sightStart and sightEnd
	}

		

		void DoPatrol () {
		transform.Translate (Vector3.right * Time.deltaTime * speed);							// gives the illusion of walking along the vector

		if (Vector3.Distance (transform.position, currentPatrolPoint.position) < .1f) {			// measures the distance between the enemy and the nearest checkpoint to see where to go next
			if (currentPatrolIndex + 1 < patrolPoints.Length) {									// assures that the next checkpoint exists
				currentPatrolIndex++;															// goes to the next checkpoint in the list
			} else {
				currentPatrolIndex = 0;															// if there are no more checkpoints, the enemy will return to the first in the list
			}
			currentPatrolPoint = patrolPoints [currentPatrolIndex];								// sets the current patrol point equal to the list
		}

		Vector3 patrolPointDir = currentPatrolPoint.position - transform.position;				// finds the directional vector between the enemy and the current patrol point
		float angle = Mathf.Atan2 (patrolPointDir.y, patrolPointDir.x) * Mathf.Rad2Deg - 90f;	// finds the correct angle to rotate toward

		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);							// does mathematical equation to align the enemy to the vector
		transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180f);			// rotates toward the next patrol point
		}

		
		void DoCanSee () {
		Vector3 targetDir = otherObject.position - transform.position;							// finds the directional vector between the enemy and the target
		float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg - 0f;				// finds the correct angle to rotate toward
		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);							// does mathematical equation to align the enemy to the vector
		transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180);				// rotates toward the player

		transform.Translate (Vector3.right * Time.deltaTime * speed);							// gives the illusion of walking along the vector


		
	}

		
		void DoCanHear () {
		transform.Rotate (0, 0, rotationSpeed);												// obtains the AI's position so that it can rotate and better find the player
		}

	}
