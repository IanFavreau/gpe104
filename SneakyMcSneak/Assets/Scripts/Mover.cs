﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public Transform tf;							// sets tf to public to be accessed by the game developer of type "Transform", which is a component
	public float speed;								// sets speed to public to be accessed by the game developer of type "float"
	public float turnSpeed;							// sets turnSpeed to public to be accessed by the game developer of type "float"

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();			// accesses the Transform component while assigning it to the established variable "tf"


	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W)) {				// reads in user input of cardinal direction corresponding to the key "W", Forward
			tf.position += tf.right * speed; 		// use tf.right instead of Vector3 for local right

			if (Input.GetKey (KeyCode.A)) {			// reads in user input of cardinal direction corresponding to the key "A", Left
				tf.Rotate (0, 0, turnSpeed);		// attributes a positive turnSpeed value to allow the game object to rotate toward the left
			}

			if (Input.GetKey (KeyCode.D)) {			// reads in user input of cardinal direction corresponding to the key "D", Right
				tf.Rotate (0, 0, -turnSpeed);		// attributes a negative turnSpeed value to allow the game object to rotate toward the right
			}
		}

		if (Input.GetKey (KeyCode.S)) {				// reads in user input of cardinal direction corresponding to the key "S", Back
			tf.position -= tf.right * speed;		// adjusts the game object's position to fit the player's input with respect to the speed variable

			if (Input.GetKey (KeyCode.A)) {			// reads in user input of cardinal direction corresponding to the key "A", Left while holding sown the Back directional key
				tf.Rotate (0, 0, -turnSpeed);		// attributes a negative turnSpeed value to allow the game object to rotate toward the right
			}

			if (Input.GetKey (KeyCode.D)) {			// reads in user input of cardinal direction corresponding to the key "D", Right while holding sown the Back directional key
				tf.Rotate (0, 0, turnSpeed);		// attributes a positive turnSpeed value to allow the game object to rotate toward the left
			}
		}




		if (Input.GetKey (KeyCode.A) && !Input.GetKey (KeyCode.S)) {		// reads in player's directional input while they also press the "S" directional key
			tf.Rotate (0, 0, turnSpeed);									// attributes a positive turnSpeed value to allow the game object to rotate toward the left
		}


		if (Input.GetKey (KeyCode.D) && !Input.GetKey (KeyCode.S)) {		// reads in player's directional input while they also press the "S" directional key
			tf.Rotate (0, 0, -turnSpeed);									// attributes a negative turnSpeed value to allow the game object to rotate toward the Right
		}

	}
}
