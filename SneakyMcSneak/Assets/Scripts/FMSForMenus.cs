﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMSForMenus : MonoBehaviour {

	public enum MenuStates { Start, Gameplay, GameOver};				// Creates a public variable "MenuStates", taking in the parameters of Start, Gameplay, and GameOver
	public MenuStates currentState;										// Creates a public variable "tf", initializing it to currentState


	// Use this for initialization
	void Start () {
		
	}

	void Update () {

		switch (currentState) {											// sets up a switch statement that will transition between each of the states
		case MenuStates.Start:											// determines the first state of the menus, the Start screen
			// Do work
			DoStart ();													// calls the DoStart Function
			//check for button press
			break;
		case MenuStates.Gameplay:										// determines the second state of the menus, the Gameplay screen
			// Do work
			DoGameplay ();												// calls the DoGameplay Function
			//check for lose condition
			break;
		case MenuStates.GameOver:										// determines the final state of the menus, the Game Over screen
			// Do work
			DoGameOver ();												// calls the DoGameOver Function
			//check for play again button press
			break;
		}
}

	void DoStart () {
		// load the next screen in the game manager
	}

	void DoGameplay () {
		// load the next screen in the game manager
	}

	void DoGameOver () {
		// load the next screen in the game manager
	}
}
